<?php  
//Abraham Hernandez Medina

	class token{
		//declaracion de atributos
		private $nombre;
		private $token;
		//Constructor
		public function __construct($nombre_front){
			$this->nombre=$nombre_front;
			$this->token=rand();
		}
		
		public function mostrar(){
			return 'Saludos '.$this->nombre.' este es tu token: '.$this->token;
		}

		//Destructor
		public function __destruct(){
			$this->token='El token ha sido destruido';
			echo $this->token;
		}
	}

$mensaje='';


if (!empty($_POST)){
	//creacion de objeto de la clase
	$token1= new token($_POST['nombre']);
	$mensaje=$token1->mostrar();
}


?>
