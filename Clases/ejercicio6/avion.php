<?php
include_once('transporte.php');
//Abraham Hernandez M.

class avion extends transporte{
    private $numero_turbinas;

    //Constructor
    public function __construct($nom,$vel,$com,$tur){
        parent::__construct($nom,$vel,$com);
        $this->numero_turbinas=$tur;
    }

    //Método
    public function resumenAvion(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Numero de turbinas:</td>
                    <td>'. $this->numero_turbinas.'</td>				
                </tr>';
        return $mensaje;
    }
}

$msgAvion='';

if (!empty($_POST)){
    switch ($_POST['tipo_transporte']) {
        case 'aereo':
            $jet1= new avion('jet','400','gasoleo','2');
            $msgAvion=$jet1->resumenAvion();
            break;		
    }

}

?>
