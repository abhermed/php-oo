<?php
include_once('transporte.php');
//Abraham Hernandez M
class Rustico extends transporte{
    private $clase;

    //Constructor
    public function __construct($nom,$vel,$com,$cls){
        parent::__construct($nom,$vel,$com);
        $this->clase=$cls;
    }

    //Método
    public function resumenBici(){
        $mensaje=parent::crear_ficha();
        $mensaje.='<tr>
                    <td>Tipo de bicicleta:</td>
                    <td>'. $this->clase.'</td>				
                </tr>';      
        return $mensaje;
    }
}

$msgBicicleta='';    

if (!empty($_POST)){
    switch ($_POST['tipo_transporte']) {
        case 'rustico':
            $bici1= new Rustico('Bicicleta','20','Cinético','Deportiva');
            $msgBicicleta=$bici1->resumenBici();
            break;		
    }
}

?>
