<?php  
include_once('transporte.php');

	class carro extends transporte{

		private $numero_puertas;

		public function __construct($nom,$vel,$com,$pue){
			parent::__construct($nom,$vel,$com);
			$this->numero_puertas=$pue;
				
		}

		//Método
		public function resumenCarro(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de puertas:</td>
						<td>'. $this->numero_puertas.'</td>				
					</tr>';
			return $mensaje;
		}
	} 

	class avion extends transporte{

		private $numero_turbinas;
		
		public function __construct($nom,$vel,$com,$tur){
			parent::__construct($nom,$vel,$com);
			$this->numero_turbinas=$tur;
		}

		public function resumenAvion(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Numero de turbinas:</td>
						<td>'. $this->numero_turbinas.'</td>				
					</tr>';
			return $mensaje;
		}
	}

	class barco extends transporte{
		private $calado;

		public function __construct($nom,$vel,$com,$cal){
			parent::__construct($nom,$vel,$com);
			$this->calado=$cal;
		}

		public function resumenBarco(){
			$mensaje=parent::crear_ficha();
			$mensaje.='<tr>
						<td>Calado:</td>
						<td>'. $this->calado.'</td>				
					</tr>';
			return $mensaje;
		}
	}

$mensaje='';


if (!empty($_POST)){
	switch ($_POST['tipo_transporte']) {
		case 'aereo':
			$jet1= new avion('jet','400','gasoleo','2');
			$mensaje=$jet1->resumenAvion();
			break;
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$mensaje=$carro1->resumenCarro();
			break;
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$mensaje=$bergantin1->resumenBarco();
			break;		
	}

}

?>
