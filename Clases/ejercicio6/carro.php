<?php  
include_once('transporte.php');
//Abraham Hernandez M
class carro extends transporte{
	private $numero_puertas;

	//Constructor
	public function __construct($nom,$vel,$com,$pue){
		parent::__construct($nom,$vel,$com);
		$this->numero_puertas=$pue;
	}

	//Método
	public function resumenCarro(){
		$mensaje=parent::crear_ficha();
		$mensaje.='<tr>
					<td>Numero de puertas:</td>
					<td>'. $this->numero_puertas.'</td>				
				</tr>';
		return $mensaje;
	}
} 

$msgCarro='';

if(!empty($_POST)){
	switch ($_POST['tipo_transporte']) {
		case 'terrestre':
			$carro1= new carro('carro','200','gasolina','4');
			$msgCarro=$carro1->resumenCarro();
			break;	
	}

}    

?>
