<?php
//Abraham Hernandez M.
include_once('transporte.php');

class barco extends transporte{
	private $calado;

	//Constructor
	public function __construct($nom,$vel,$com,$cal){
		parent::__construct($nom,$vel,$com);
		$this->calado=$cal;
	}

	//Método
	public function resumenBarco(){
		$mensaje=parent::crear_ficha();
		$mensaje.='<tr>
					<td>Calado:</td>
					<td>'. $this->calado.'</td>				
				</tr>';
		return $mensaje;
	}
}

$msgBarco='';

if (!empty($_POST)){
    switch ($_POST['tipo_transporte']) {
		case 'maritimo':
			$bergantin1= new barco('bergantin','40','na','15');
			$msgBarco=$bergantin1->resumenBarco();
			break;		
	    }

    }    

?>
