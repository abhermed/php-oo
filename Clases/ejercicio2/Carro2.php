<?php
//creación de la clase carro
//Abraham Hernandez Medina

class Carro2{
	//declaracion de propiedades
	public $color;
	public $modelo;
	public $fab;
	//verificacion
	public $circulacion; 

	//Fución verificacion 
	public function verificacion($anio){
		if ($anio < 1990){
			$this->circulacion = "No";
		}elseif($anio >= 1990 && $anio <= 2010){
			$this->circulacion = "En revisión";
		}else{
			$this->circulacion = "Si";
		}
	}

	public function get_estado(){
		return $this->circulacion;
	}
}

//Instancia
$CarroNew = new Carro2();

if (!empty($_POST)){
	//Listado
	$CarroNew->color=$_POST['color'];
	$CarroNew->modelo=$_POST['modelo'];
	$CarroNew->fab=$_POST['anio'];
	//Verificacion
	$anio_fabricacion = ($_POST['anio']);
	$CarroNew->verificacion($anio_fabricacion);
}


