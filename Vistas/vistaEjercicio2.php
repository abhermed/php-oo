<?php
//comando de inclusion con la ruta de las clases
include_once('../Clases/ejercicio2/Carro2.php');
?>

<!DOCTYPE html>
<!-- Abraham Hernandez M. -->
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		2 - Abraham Hernandez M.
	</title>
</head>
<body>
	<!-- Labels -->
	<div class="container" style="margin-top: 4em">
	<header> <h1>Verificación</h1></header><br>
	<form method="post">
		<div class="form-group row">
			 <label class="col-sm-3" for="CajaTexto1">Color:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>

			<div class="col-sm-4">
			</div>

			<label class="col-sm-3" for="CajaTexto2">Modelo:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="modelo" id="CajaTexto2">
			</div>
			
			<div class="col-sm-4">
			</div>

			<label class="col-sm-3" for="CajaTexto3">Año: </label>
			<div class="col-sm-4">				
				<input class="form-control" type='number' name="anio" id="CajaTexto3">			
			</div>
		</div>
		<button class="btn btn-primary" type="submit" >Enviar</button>
		<a class="btn btn-link offset-md-9 offset-lg-9 offset-7" href="../index.php">Regresar</a>
	</form>
 
	<!-- Respuesta -->
	</div>
	<div class="container mt-5">
		<h1>Respuesta del servidor</h1>
		<table class="table">
				<thead>
		      <tr>
		       <th>Caracteristicas del carro</th>
		      </tr>
		    </thead>
		    <tbody>
					<tr>
						<td><?='Color:'?></td>
						<td><?=$CarroNew->color?></td>
													
					</tr>
				
					<tr>
						<td><?='Modelo:'?></td>
						<td><?=$CarroNew->modelo?></td>
												
					</tr>

					<tr>
						<td><?='Año:'?></td>
						<td><?=$CarroNew->fab?></td>
												
					</tr>

					<tr>
						<td><?='Circula:'?></td>
						<td><?=$CarroNew->get_estado()?></td>
												
					</tr>	
			</tbody>
		</table>
</body>
</html>
