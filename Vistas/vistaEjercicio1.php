<?php
	//Include Carro y Moto
	//Abraham Hernandez Medina
    include('../Clases/ejercicio1/Carro.php');
    include('../Clases/ejercicio1/Moto.php');
?>

<!DOCTYPE html>
<html>
<head>

	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/bootstrap-grid.css">
	<script type="text/javascript" src="../js/bootstrap.min.js"></script>
	<script type="text/javascript" src="../js/jquery-3.4.1.min.js"></script>
	<title>
		Abraham Hernandez M.
	</title>
</head>
<body>
	<div class="container" style="margin-top: 4em">
	
	<header> <h1>Carro y Moto</h1></header><br>
	<form method="post">
		<div class="form-group row">
			<!--Labels Auto-->
			 <label class="col-sm-3" for="CajaTexto1">Color del carro:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="color" name="color" id="CajaTexto1">
			</div>
			<div class="col-sm-4">
			</div>

			<!--Labels Moto-->
            <label class="col-sm-3" for="CajaTexto1">Marca de la moto:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="marca" id="CajaTexto2" placeholder = "Ingresa una marca">
			</div>
			<div class="col-sm-4">
			</div> 
			
            <label class="col-sm-3" for="CajaTexto1">Modelo de la moto:</label>
			 <div class="col-sm-4">
					<input class="form-control" type="text" name="modelo" id="CajaTexto3" placeholder = "Ingresa un año">
			</div>
			<div class="col-sm-4">
			</div>

						
		</div>
		<button class="btn btn-primary" type="submit">Enviar</button>
		<a class="btn btn-link offset-md-8 offset-lg-9 offset-6" href="../index.php">Regresar</a>
	</form>
	</div>
	<br>
	<!--Mensajes del servidor-->
	<input type="text" class="form-control" value="<?php  echo $mensajeServidor; ?>" readonly>
	<br>
    <input type="text" class="form-control" value="<?php  echo $mensajeServidorMt; ?>" readonly>
</body>
</html>
